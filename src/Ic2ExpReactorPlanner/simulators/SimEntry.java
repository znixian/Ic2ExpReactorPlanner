/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ic2ExpReactorPlanner.simulators;

/**
 *
 * @author znix
 */
public class SimEntry {

	public final String data;
	public final boolean log;

	public SimEntry(String data, boolean log) {
		this.data = data;
		this.log = log;
	}
}
