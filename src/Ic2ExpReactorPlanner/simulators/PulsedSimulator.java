package Ic2ExpReactorPlanner.simulators;

import Ic2ExpReactorPlanner.Reactor;
import Ic2ExpReactorPlanner.ReactorPlannerFrame;
import java.util.Arrays;
import javax.swing.JPanel;

/**
 *
 * @author Brian McCloud
 */
public class PulsedSimulator extends BasicSimulator {

	private final boolean[][] alreadyBroken = new boolean[6][9];

	private final int onPulseDuration;

	private final int offPulseDuration;

	private final int suspendTemp;

	private final int resumeTemp;

	private boolean active = true;

	private int nextOffTime = 0;

	private int nextOnTime = 0;

	public PulsedSimulator(Reactor reactor, ReactorPlannerFrame output,
			JPanel[][] reactorButtonPanels, int initialHeat,
			int onPulseDuration, int offPulseDuration,
			int suspendTemp, int resumeTemp) {
		super(reactor, output, reactorButtonPanels, initialHeat);
		this.onPulseDuration = onPulseDuration;
		this.offPulseDuration = offPulseDuration;
		this.suspendTemp = suspendTemp;
		this.resumeTemp = resumeTemp;
		this.nextOffTime = onPulseDuration;
	}

	@Override
	protected Void doInBackground() throws Exception {
		long startTime = System.nanoTime();
		ReactorState state = new ReactorState();
		ReactorCooldownState cooldownState = new ReactorCooldownState();
		try {
			initSimulation(state);
			do {
				if (active) {
					simulateTick(state);
				} else {
					simulateCooldownTick(state, cooldownState);
				}
				advancePulseCounter(state);
			} while (shouldRun(state));
			showMinMaxHeat(state);
			if (reactor.getCurrentHeat() <= reactor.getMaxHeat()) {
				showIntactOutput(state);
			} else {
				showInfo(String.format(b().getString("REACTOR_OVERHEATED_TIME"), state.reactorTicks));
			}

			showHeatStats(state);
		} catch (Throwable e) {
			if (state.cooldownTicks == 0) {
				showInfo(String.format(b().getString("ERROR_AT_REACTOR_TICK"), state.reactorTicks));
			} else {
				showInfo(String.format(b().getString("ERROR_AT_COOLDOWN_TICK"), state.cooldownTicks));
			}
			showInfo(e.toString(), " ", Arrays.toString(e.getStackTrace()));
		}
		long endTime = System.nanoTime();
		showInfo(String.format(b().getString("SIMULATION_TIME"), (endTime - startTime) / 1e9));
		return null;
	}

	private boolean shouldRun(ReactorState state) {
		if (isCancelled()) {
			return false;
		}
		double heat = reactor.getCurrentHeat();
		if (heat > reactor.getMaxHeat()) {
			return false;
		}
		if (active && state.lastEUoutput <= 0) {
			return false;
		}
		if (!active && (reactor.getVentedHeat() == 0 && heat > 0)) {
			return false;
		}
		return true;
	}

	private void advancePulseCounter(ReactorState state) {
		if (active) {
			if (reactor.getCurrentHeat() >= suspendTemp || state.reactorTicks >= nextOffTime) {
				active = false;
				nextOnTime = state.reactorTicks + offPulseDuration;
			}
		} else if (reactor.getCurrentHeat() <= resumeTemp && state.reactorTicks >= nextOnTime) {
			active = true;
			nextOffTime = state.reactorTicks + onPulseDuration;
		}
	}
}
