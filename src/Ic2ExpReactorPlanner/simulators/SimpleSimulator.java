package Ic2ExpReactorPlanner.simulators;

import Ic2ExpReactorPlanner.Reactor;
import Ic2ExpReactorPlanner.ReactorPlannerFrame;
import Ic2ExpReactorPlanner.items.ReactorComponent;
import Ic2ExpReactorPlanner.items.fuelrods.IFuelRod;
import java.util.Arrays;
import javax.swing.JPanel;

/**
 *
 * @author Brian McCloud
 */
public class SimpleSimulator extends BasicSimulator {

	public SimpleSimulator(Reactor reactor, ReactorPlannerFrame output,
			JPanel[][] reactorButtonPanels, int initialHeat) {
		super(reactor, output, reactorButtonPanels, initialHeat);
	}

	@Override
	protected Void doInBackground() throws Exception {
		long startTime = System.nanoTime();
		ReactorState state = new ReactorState();
		ReactorClassification classification = new ReactorClassification();
		// start at the worst one, then filter down
		classification.setStability(ReactorClassification.Stability.V);
		try {
			initSimulation(state);
			int cycles = 0;
			reactor.startSimulation();
			do {
				simulateTick(state);
				if (state.reactorTicks == 75 && !reactor.hasStateChanged()
						&& reactor.getCurrentHeat() == state.lastReactorHeat) {
					// Mark I
					classification.setStability(ReactorClassification.Stability.I);
					break;
				}
				if (state.reactorTicks == 10000 / 10) {
					// 10% of the way, min for Mk III and Mk IV
					if (reactor.hasComponentFailed()) {
						classification.setMinimumStability(ReactorClassification.Stability.IV);
					} else {
						classification.setMinimumStability(ReactorClassification.Stability.III);
					}
				}
				if (state.lastEUoutput <= 0.0) {
					// At most Mark II
					classification.setMinimumStability(ReactorClassification.Stability.II);
					cycles++;
					resetFuel();
					if (cycles >= 16) {
						// Mark II-E
						classification.setStability(ReactorClassification.Stability.II_E);
						break;
					}
				}
			} while (reactor.getCurrentHeat() <= reactor.getMaxHeat());
			classification.setSingleUseCoolant(reactor.hasSingleUseCoolant());
			if (reactor.isFluid()) {
				classification.setFluidReactor(true);
			} else {
				classification.setFluidReactor(false);
				int efficiency_i = (int) (state.totalHeatOutput
						/ state.reactorTicks / 4 / state.totalRodCount);
				ReactorClassification.Efficiency efficiency
						= ReactorClassification.Efficiency
						.fromEfficiencyValue(efficiency_i);
				classification.setEfficiency(efficiency);
			}
			if (state.reactorTicks > 0) {
				showEfficiencyOutput(state);
			}
			showInfo(String.format(
					b().getString("CLASSIFICATION"),
					classification
			));
			showMinMaxHeat(state);
			if (reactor.getCurrentHeat() <= reactor.getMaxHeat()) {
				showIntactOutput(state);
			} else {
				showInfo(String.format(b().getString("REACTOR_OVERHEATED_TIME"), state.reactorTicks));
			}

			showHeatStats(state);
		} catch (Throwable e) {
			if (state.cooldownTicks == 0) {
				showInfo(String.format(b().getString("ERROR_AT_REACTOR_TICK"), state.reactorTicks));
			} else {
				showInfo(String.format(b().getString("ERROR_AT_COOLDOWN_TICK"), state.cooldownTicks));
			}
			showInfo(e.toString(), " ", Arrays.toString(e.getStackTrace()));
		}
		long endTime = System.nanoTime();
		showInfo(String.format(b().getString("SIMULATION_TIME"), (endTime - startTime) / 1e9));
		return null;
	}

	private void resetFuel() {
		for (int row = 0; row < 6; row++) {
			for (int col = 0; col < 9; col++) {
				ReactorComponent component = reactor.getComponentAt(row, col);
				if ((component instanceof IFuelRod)) {
					component.clearDamage();
				}
			}
		}
	}
}
