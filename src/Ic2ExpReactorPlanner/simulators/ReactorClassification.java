/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ic2ExpReactorPlanner.simulators;

/**
 *
 * @author znix
 */
public class ReactorClassification {

	private boolean singleUseCoolant;
	private boolean fluidReactor;
	private Stability stability = Stability.V;
	private Efficiency efficiency = Efficiency.EE;

	public ReactorClassification() {
	}

	public Stability getStability() {
		return stability;
	}

	public void setStability(Stability stability) {
		if (stability == null) {
			throw new IllegalArgumentException();
		}
		this.stability = stability;
	}

	public void setMinimumStability(Stability stability) {
		if (stability == null) {
			throw new IllegalArgumentException();
		}
		if (this.stability.ordinal() > stability.ordinal()) {
			this.stability = stability;
		}
	}

	public Efficiency getEfficiency() {
		return efficiency;
	}

	public void setEfficiency(Efficiency efficiency) {
		if (stability == null) {
			throw new IllegalArgumentException();
		}
		this.efficiency = efficiency;
	}

	public boolean isSingleUseCoolant() {
		return singleUseCoolant;
	}

	public void setSingleUseCoolant(boolean singleUseCoolant) {
		this.singleUseCoolant = singleUseCoolant;
	}

	public boolean isFluidReactor() {
		return fluidReactor;
	}

	public void setFluidReactor(boolean fluidReactor) {
		this.fluidReactor = fluidReactor;
	}

	@Override
	public String toString() {
		return stability + " " + efficiency
				+ (singleUseCoolant ? "-SUC" : "")
				+ (fluidReactor ? "-LQR" : "");
	}

	/**
	 * Possible stabilities
	 */
	public static enum Stability {
		I,
		II_E("II-E"),
		II,
		III,
		IV,
		V;

		private final String printable;

		private Stability(String printable) {
			this.printable = printable;
		}

		private Stability() {
			this.printable = name();
		}

		@Override
		public String toString() {
			return printable;
		}
	}

	/**
	 * Possible efficiency values
	 */
	public static enum Efficiency {
		EE,
		ED,
		EC,
		EB,
		EA,
		EA_P("EA+"),
		EA_PP("EA++"),
		EA_STAR("EA*");

		private final String printable;

		private Efficiency(String printable) {
			this.printable = printable;
		}

		private Efficiency() {
			this.printable = name();
		}

		@Override
		public String toString() {
			return printable;
		}

		public static Efficiency fromEfficiencyValue(double value) {
			if (value == 1) {
				return EE;
			}
			return values()[(int) value];
		}
	}
}
