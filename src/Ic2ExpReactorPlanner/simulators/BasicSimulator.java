/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ic2ExpReactorPlanner.simulators;

import Ic2ExpReactorPlanner.Reactor;
import Ic2ExpReactorPlanner.items.ReactorComponent;
import Ic2ExpReactorPlanner.ReactorPlannerFrame;
import java.awt.Color;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;

/**
 *
 * @author znix
 */
public class BasicSimulator extends SwingWorker<Void, SimEntry> {

	protected final Reactor reactor;

	private final JTextArea outputMain;
	private final JTextArea outputLog;

	private final JPanel[][] reactorButtonPanels;

	protected final int initialHeat;

	private double minEUoutput = Double.MAX_VALUE;

	private double maxEUoutput = 0.0;

	private double minHeatOutput = Double.MAX_VALUE;

	private double maxHeatOutput = 0.0;

	protected final boolean[][] alreadyBroken = new boolean[6][9];

	protected final boolean[][] needsCooldown = new boolean[6][9];

	private int lastLogTime;

	public BasicSimulator(final Reactor reactor, final ReactorPlannerFrame output, final JPanel[][] reactorButtonPanels, final int initialHeat) {
		this.reactor = reactor;
		this.reactorButtonPanels = reactorButtonPanels;
		this.initialHeat = initialHeat;

		outputMain = output.getOutputArea();
		outputLog = output.getLogArea();
	}

	@Override
	protected Void doInBackground() throws Exception {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	protected void registerEUOutput(double eu) {
		minEUoutput = Math.min(eu, minEUoutput);
		maxEUoutput = Math.max(eu, maxEUoutput);
	}

	protected void registerHeatOutput(double heat) {
		minHeatOutput = Math.min(heat, minHeatOutput);
		maxHeatOutput = Math.max(heat, maxHeatOutput);
	}

	protected void showInfo(String... data) {
		for (String string : data) {
			publish(new SimEntry(string, false));
		}
	}

	protected void showLog(ReactorState reactorState, String data) {
		int deltaTime = reactorState.reactorTicks - lastLogTime;
		if (deltaTime > 1 && lastLogTime != -1) {
			publish(new SimEntry(String.format(b().getString("Log.GAP"),
					deltaTime
			), true));
		}
		lastLogTime = reactorState.reactorTicks;
		publish(new SimEntry(data + '\n', true));
	}

	protected void showBoth(String data) {
		showInfo(data);
		publish(new SimEntry(data, true));
	}

	@Override
	protected void process(List<SimEntry> chunks) {
		if (!isCancelled()) {
			for (SimEntry data : chunks) {
				JTextArea area = data.log ? outputLog : outputMain;
				String chunk = data.data;
				if (chunk.isEmpty()) {
					area.setText("");
				} else if (chunk.matches("R\\dC\\d:.*")) { //NOI18N
					String temp = chunk.substring(5);
					int row = chunk.charAt(1) - '0';
					int col = chunk.charAt(3) - '0';
					if (temp.startsWith("0x")) { //NOI18N
						reactorButtonPanels[row][col].setBackground(Color.decode(temp));
					} else if (temp.startsWith("+")) { //NOI18N
						final ReactorComponent component = reactor.getComponentAt(row, col);
						if (component != null) {
							component.info += "\n" + temp.substring(1); //NOI18N
						}
					} else {
						final ReactorComponent component = reactor.getComponentAt(row, col);
						if (component != null) {
							component.info = temp;
						}
					}
				} else {
					area.append(chunk);
				}
			}
		}
	}

	// Simulators
	protected void initSimulation(ReactorState state) {
		lastLogTime = -1;
		showBoth("");
		showBoth(b().getString("SIMULATION_STARTED"));
		reactor.setCurrentHeat(initialHeat);
		reactor.clearVentedHeat();

		// reset everything
		for (int row = 0; row < 6; row++) {
			for (int col = 0; col < 9; col++) {
				ReactorComponent component = reactor.getComponentAt(row, col);
				// reset the component
				if (component != null) {
					component.clearCurrentHeat();
					component.clearDamage();
					state.totalRodCount += component.getRodCount();
				}

				// reset the heat marker
				showInfo(String.format("R%dC%d:0xC0C0C0", row, col)); //NOI18N
			}
		}
	}

	protected void resetSimulatedTick(ReactorState state) {
		state.reactorTicks++;
		state.lastReactorHeat = reactor.getCurrentHeat();
		reactor.clearEUOutput();
		reactor.clearVentedHeat();
		for (int row = 0; row < 6; row++) {
			for (int col = 0; col < 9; col++) {
				ReactorComponent component = reactor.getComponentAt(row, col);
				if (component != null) {
					component.preReactorTick();
				}
			}
		}
	}

	protected void postTick(ReactorState state) {
		if (!state.reachedExplode) {
			boolean logged = false;
			if (!state.stateLoggedPost) {
				logDataNow(state);
				state.stateLoggedPost = true;
				logged = true;
			}
			if (!state.stateLogged) {
				if (!logged) {
					logDataNow(state);
				}
				state.stateLogged = true;
				state.stateLoggedPost = false;
			}
		}
	}

	private void logDataNow(ReactorState state) {
		double heat = reactor.getCurrentHeat();
		showLog(state, String.format(b().getString("Log.STATE"),
				state.reactorTicks,
				(int) heat,
				(int) (heat - state.lastReactorHeat),
				(int) reactor.getVentedHeat()
		));
	}

	public void simulateTick(ReactorState state) {
		resetSimulatedTick(state);
		double generatedHeat = 0.0;
		for (int row = 0; row < 6; row++) {
			for (int col = 0; col < 9; col++) {
				ReactorComponent component = reactor.getComponentAt(row, col);
				if (component != null && !component.isBroken()) {
					double componentHeat = component.generateHeat();
					generatedHeat += componentHeat;
					component.registerGeneratedHeat(componentHeat);
					state.maxReactorHeat = Math.max(reactor.getCurrentHeat(), state.maxReactorHeat);
					state.minReactorHeat = Math.min(reactor.getCurrentHeat(), state.minReactorHeat);
					component.dissipate();
					state.maxReactorHeat = Math.max(reactor.getCurrentHeat(), state.maxReactorHeat);
					state.minReactorHeat = Math.min(reactor.getCurrentHeat(), state.minReactorHeat);
					component.transfer();
					state.maxReactorHeat = Math.max(reactor.getCurrentHeat(), state.maxReactorHeat);
					state.minReactorHeat = Math.min(reactor.getCurrentHeat(), state.minReactorHeat);
				}
				double maxHeat = state.maxReactorHeat;
				int maxHeatDisp = (int) maxHeat;
				if (maxHeat >= 0.4 * reactor.getMaxHeat() && !state.reachedBurn) {
					showInfo(String.format(b().getString("REACTOR_BURN_TIME"), state.reactorTicks));
					showLog(state, String.format(b().getString("Log.REACTOR_BURN"), state.reactorTicks, maxHeatDisp));
					state.reachedBurn = true;
					state.stateLoggedPost = true;
				}
				if (maxHeat >= 0.5 * reactor.getMaxHeat() && !state.reachedEvaporate) {
					showInfo(String.format(b().getString("REACTOR_EVAPORATE_TIME"), state.reactorTicks));
					showLog(state, String.format(b().getString("Log.REACTOR_EVAPORATE"), state.reactorTicks, maxHeatDisp));
					state.reachedEvaporate = true;
					state.stateLoggedPost = true;
				}
				if (maxHeat >= 0.7 * reactor.getMaxHeat() && !state.reachedHurt) {
					showInfo(String.format(b().getString("REACTOR_HURT_TIME"), state.reactorTicks));
					showLog(state, String.format(b().getString("Log.REACTOR_HURT"), state.reactorTicks, maxHeatDisp));
					state.reachedHurt = true;
					state.stateLoggedPost = true;
				}
				if (maxHeat >= 0.85 * reactor.getMaxHeat() && !state.reachedLava) {
					showInfo(String.format(b().getString("REACTOR_LAVA_TIME"), state.reactorTicks));
					showLog(state, String.format(b().getString("Log.REACTOR_LAVA"), state.reactorTicks, maxHeatDisp));
					state.reachedLava = true;
					state.stateLoggedPost = true;
				}
				if (maxHeat >= reactor.getMaxHeat() && !state.reachedExplode) {
					showInfo(String.format(b().getString("REACTOR_EXPLODE_TIME"), state.reactorTicks));
					showLog(state, String.format(b().getString("Log.REACTOR_EXPLODE"), state.reactorTicks, maxHeatDisp));
					state.reachedExplode = true;
				}
			}
		}
		state.maxGeneratedHeat = Math.max(generatedHeat, state.maxGeneratedHeat);
		for (int row = 0; row < 6; row++) {
			for (int col = 0; col < 9; col++) {
				ReactorComponent component = reactor.getComponentAt(row, col);
				if (component != null && !component.isBroken()) {
					component.generateEnergy();
				}
			}
		}
		state.lastEUoutput = reactor.getCurrentEUoutput();
		state.totalEUoutput += state.lastEUoutput;
		state.lastHeatOutput = reactor.getVentedHeat();
		state.totalHeatOutput += state.lastHeatOutput;
		if (reactor.getCurrentHeat() <= reactor.getMaxHeat() && state.lastEUoutput > 0.0) {
			registerEUOutput(state.lastEUoutput);
			registerHeatOutput(state.lastHeatOutput);
		}
		for (int row = 0; row < 6; row++) {
			for (int col = 0; col < 9; col++) {
				ReactorComponent component = reactor.getComponentAt(row, col);
				if (component != null && component.isBroken() && !alreadyBroken[row][col] && !component.getClass().getName().contains("FuelRod")) { //NOI18N
					showInfo(String.format("R%dC%d:0xFF0000", row, col)); //NOI18N
					alreadyBroken[row][col] = true;
					showInfo(String.format(b().getString("BROKE_TIME"), row, col, state.reactorTicks));
					showLog(state, String.format(b().getString("Log.ITEM_BROKE"),
							state.reactorTicks, row, col, component));
					state.stateLogged = false;
				}
			}
		}
		postTick(state);
	}

	public void simulateCooldownTick(ReactorState state, ReactorCooldownState cooldown) {
		resetSimulatedTick(state);
		cooldown.prevReactorHeat = reactor.getCurrentHeat();
		if (cooldown.prevReactorHeat == 0.0) {
			cooldown.reactorCooldownTime = state.cooldownTicks;
		}
		cooldown.prevTotalComponentHeat = cooldown.currentTotalComponentHeat;
		for (int row = 0; row < 6; row++) {
			for (int col = 0; col < 9; col++) {
				ReactorComponent component = reactor.getComponentAt(row, col);
				if (component != null && !component.isBroken()) {
					component.dissipate();
					component.transfer();
				}
			}
		}
		state.lastHeatOutput = reactor.getVentedHeat();
		state.totalHeatOutput += state.lastHeatOutput;
		registerEUOutput(state.lastEUoutput);
		registerHeatOutput(state.lastHeatOutput);
		state.cooldownTicks++;
		cooldown.currentTotalComponentHeat = 0.0;
		for (int row = 0; row < 6; row++) {
			for (int col = 0; col < 9; col++) {
				ReactorComponent component = reactor.getComponentAt(row, col);
				if (component != null && !component.isBroken()) {
					cooldown.currentTotalComponentHeat += component.getCurrentHeat();
					if (component.getCurrentHeat() == 0.0 && needsCooldown[row][col]) {
						showInfo(String.format(b().getString("COMPONENT_COOLDOWN_TIME"), row, col, state.cooldownTicks));
						needsCooldown[row][col] = false;
					}
				}
			}
		}
		postTick(state);
	}

	// Outputs
	protected void showIntactOutput(ReactorState state) {
		showInfo(String.format(b().getString("FUEL_RODS_TIME"), state.reactorTicks));
		state.lastHeatOutput = 0.0;
		state.totalHeatOutput = 0.0;
		ReactorCooldownState cooldownState = new ReactorCooldownState();
		cooldownState.prevReactorHeat = reactor.getCurrentHeat();
		for (int row = 0; row < 6; row++) {
			for (int col = 0; col < 9; col++) {
				ReactorComponent component = reactor.getComponentAt(row, col);
				if (component != null && !component.isBroken()) {
					cooldownState.prevTotalComponentHeat += component.getCurrentHeat();
					if (component.getCurrentHeat() > 0.0) {
						// mark it as heated
						showInfo(String.format("R%dC%d:0xFFFF00", row, col));
						showInfo(String.format(b().getString("COMPONENT_REMAINING_HEAT"), row, col, component.getCurrentHeat()));
						needsCooldown[row][col] = true;
					}
				}
			}
		}
		if (cooldownState.prevReactorHeat == 0.0 && cooldownState.prevTotalComponentHeat == 0.0) {
			showInfo(b().getString("NO_COOLDOWN_NEEDED"));
		} else {
			cooldownState.currentTotalComponentHeat = cooldownState.prevTotalComponentHeat;
			do {
				simulateCooldownTick(state, cooldownState);
			} while (state.lastHeatOutput > 0 && state.cooldownTicks < 20000);
			if (reactor.getCurrentHeat() == 0.0) {
				showInfo(String.format(b().getString("REACTOR_COOLDOWN_TIME"),
						cooldownState.reactorCooldownTime));
			} else {
				showInfo(String.format(b().getString("REACTOR_RESIDUAL_HEAT"),
						reactor.getCurrentHeat(),
						cooldownState.reactorCooldownTime));
			}
			showInfo(String.format(b().getString("TOTAL_COOLDOWN_TIME"), state.cooldownTicks));
			for (int row = 0; row < 6; row++) {
				for (int col = 0; col < 9; col++) {
					ReactorComponent component = reactor.getComponentAt(row, col);
					if (component != null && !component.isBroken()) {
						cooldownState.prevTotalComponentHeat += component.getCurrentHeat();
						if (component.getCurrentHeat() > 0.0) {
							// Residual Heat
							showInfo(String.format("R%dC%d:0xFFA500", row, col)); //NOI18N
							showInfo(String.format(b().getString("COMPONENT_RESIDUAL_HEAT"), row, col, component.getCurrentHeat()));
						}
					}
				}
			}
		}
	}

	protected void showEfficiencyOutput(ReactorState state) {
		if (reactor.isFluid()) {
			double minHeatOutput = getMinHeatOutput();
			double maxHeatOutput = getMaxHeatOutput();
			showInfo(String.format(
					b().getString("HEAT_OUTPUTS"),
					2 * state.totalHeatOutput,
					2 * state.totalHeatOutput / state.reactorTicks,
					2 * minHeatOutput, 2 * maxHeatOutput
			));
			if (state.totalRodCount > 0) {
				showInfo(String.format(
						b().getString("EFFICIENCY"),
						state.totalHeatOutput / state.reactorTicks / 4 / state.totalRodCount,
						minHeatOutput / 4 / state.totalRodCount,
						maxHeatOutput / 4 / state.totalRodCount
				));
			}
		} else {
			double minEUoutput = getMinEUoutput();
			double maxEUoutput = getMaxEUoutput();
			showInfo(String.format(
					b().getString("EU_OUTPUTS"),
					state.totalEUoutput,
					minEUoutput / 20.0,
					maxEUoutput / 20.0,
					state.totalEUoutput / (state.reactorTicks * 20)
			));
			if (state.totalRodCount > 0) {
				showInfo(String.format(
						b().getString("EFFICIENCY"),
						state.totalEUoutput / state.reactorTicks
						/ 100 / state.totalRodCount,
						minEUoutput / 100 / state.totalRodCount,
						maxEUoutput / 100 / state.totalRodCount
				));
			}
		}
	}

	protected void showHeatStats(ReactorState state) {
		if (reactor.isFluid() && reactor.getCurrentHeat() < reactor.getMaxHeat()) {
			showInfo(String.format(
					b().getString("HEAT_OUTPUTS"),
					2 * state.totalHeatOutput,
					2 * state.totalHeatOutput / state.cooldownTicks,
					2 * getMinHeatOutput(), 2 * getMaxHeatOutput()
			));
		}

		double totalEffectiveVentCooling = 0.0;
		double totalVentCoolingCapacity = 0.0;
		double totalCellCooling = 0.0;
		double totalCondensatorCooling = 0.0;

		for (int row = 0; row < 6; row++) {
			for (int col = 0; col < 9; col++) {
				ReactorComponent component = reactor.getComponentAt(row, col);
				if (component != null) {
					if (component.getVentCoolingCapacity() > 0) {
						showInfo(String.format(b().getString("USED_COOLING"), row, col, component.getEffectiveVentCooling(), component.getVentCoolingCapacity()));
						totalEffectiveVentCooling += component.getEffectiveVentCooling();
						totalVentCoolingCapacity += component.getVentCoolingCapacity();
					} else if (component.getBestCellCooling() > 0) {
						showInfo(String.format(b().getString("RECEIVED_HEAT"), row, col, component.getBestCellCooling()));
						totalCellCooling += component.getBestCellCooling();
					} else if (component.getBestCondensatorCooling() > 0) {
						showInfo(String.format(b().getString("RECEIVED_HEAT"), row, col, component.getBestCondensatorCooling()));
						totalCondensatorCooling += component.getBestCondensatorCooling();
					} else if (component.getMaxHeating() > 0) {
						showInfo(String.format(b().getString("PRODUCED_HEAT"), row, col, (int) component.getMaxHeating()));
					}
				}
			}
		}

		showInfo(String.format(b().getString("TOTAL_VENT_COOLING"), totalEffectiveVentCooling, totalVentCoolingCapacity));
		showInfo(String.format(b().getString("TOTAL_CELL_COOLING"), totalCellCooling));
		showInfo(String.format(b().getString("TOTAL_CONDENSATOR_COOLING"), totalCondensatorCooling));
		showInfo(String.format(b().getString("MAX_HEAT_GENERATED"), state.maxGeneratedHeat));
		double totalCooling = totalEffectiveVentCooling + totalCellCooling + totalCondensatorCooling;
		if (totalCooling >= state.maxGeneratedHeat) {
			showInfo(String.format(b().getString("EXCESS_COOLING"), totalCooling - state.maxGeneratedHeat));
		} else {
			showInfo(String.format(b().getString("EXCESS_HEATING"), state.maxGeneratedHeat - totalCooling));
		}
	}

	protected void showMinMaxHeat(ReactorState state) {
		showInfo(String.format(b().getString("MIN_TEMP"), state.minReactorHeat));
		showInfo(String.format(b().getString("MAX_TEMP"), state.maxReactorHeat));
	}

	// ACCESSORS
	public double getMinEUoutput() {
		return minEUoutput;
	}

	public double getMaxEUoutput() {
		return maxEUoutput;
	}

	public double getMinHeatOutput() {
		return minHeatOutput;
	}

	public double getMaxHeatOutput() {
		return maxHeatOutput;
	}

	public ResourceBundle b() {
		return ResourceBundle.getBundle("Ic2ExpReactorPlanner/Bundle");
	}

	protected class ReactorState {

		// init. data
		public double minReactorHeat = initialHeat;
		public double maxReactorHeat = initialHeat;
		public double lastReactorHeat = initialHeat;
		public boolean reachedBurn = initialHeat >= 0.4 * reactor.getMaxHeat();
		public boolean reachedEvaporate = initialHeat >= 0.5 * reactor.getMaxHeat();
		public boolean reachedHurt = initialHeat >= 0.7 * reactor.getMaxHeat();
		public boolean reachedLava = initialHeat >= 0.85 * reactor.getMaxHeat();
		public boolean reachedExplode = false;

		// state trackers
		public double lastEUoutput = 0.0;
		public double totalEUoutput = 0.0;
		public double lastHeatOutput = 0.0;
		public double totalHeatOutput = 0.0;
		public double maxGeneratedHeat = 0.0;

		// more state trackers
		public int reactorTicks = 0;
		public int cooldownTicks = 0;
		public int totalRodCount = 0;

		// has the current state already been reported?
		public boolean stateLogged;
		public boolean stateLoggedPost;
	}

	protected class ReactorCooldownState {

		public double prevReactorHeat;
		public int reactorCooldownTime = 0;
		public double prevTotalComponentHeat = 0.0;
		public double currentTotalComponentHeat;
	}
}
