/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ic2ExpReactorPlanner.items.heatvents;

import Ic2ExpReactorPlanner.MaterialsList;
import Ic2ExpReactorPlanner.items.ReactorComponent;

/**
 *
 * @author znix
 */
public abstract class AbstractHeatVent extends ReactorComponent {

	protected double currentDissipation;
	private double lastDissipation;
	private double lastHeat;

	@Override
	public boolean isHeatAcceptor() {
		return !isBroken();
	}

	@Override
	public void dissipate() {
		currentDissipation = Math.min(getVentCoolingCapacity(), getCurrentHeat());
		getParent().ventHeat(currentDissipation);
		adjustCurrentHeat(-currentDissipation);
		effectiveVentCooling = Math.max(effectiveVentCooling, currentDissipation);
	}

	@Override
	public abstract MaterialsList getMaterials();

	@Override
	public boolean hasStateChanged() {
		return currentDissipation != lastDissipation && lastHeat == currentHeat;
	}

	@Override
	public void preReactorTick() {
		super.preReactorTick();
		lastDissipation = currentDissipation;
		lastHeat = currentHeat;
	}

	@Override
	public abstract double getVentCoolingCapacity();
}
