package Ic2ExpReactorPlanner.items.heatvents;

import Ic2ExpReactorPlanner.MaterialsList;
import Ic2ExpReactorPlanner.TextureFactory;
import static Ic2ExpReactorPlanner.items.heatvents.AdvancedHeatVent.MATERIALS;

/**
 * Represents a heat vent.
 *
 * @author Brian McCloud
 */
public class HeatVent extends AbstractHeatVent {

	/**
	 * The filename for the image to show for the component.
	 */
	private static final String imageFilename = "reactorVent.png";     //NOI18N

	public static final MaterialsList MATERIALS = new MaterialsList(10, java.util.ResourceBundle.getBundle("Ic2ExpReactorPlanner/Bundle").getString("IRON"), 1, java.util.ResourceBundle.getBundle("Ic2ExpReactorPlanner/Bundle").getString("TIN"), 16.0 / 3, "Copper");

	/**
	 * Creates a new instance.
	 */
	public HeatVent() {
		setImage(TextureFactory.getImage(imageFilename));
		setMaxHeat(1000);
		automationThreshold = 900;
	}

	/**
	 * Gets the name of the component.
	 *
	 * @return the name of this component.
	 */
	@Override
	public String toString() {
		String result = java.util.ResourceBundle.getBundle("Ic2ExpReactorPlanner/Bundle").getString("HEAT VENT");
		if (getInitialHeat() > 0) {
			result += String.format(java.util.ResourceBundle.getBundle("Ic2ExpReactorPlanner/Bundle").getString("INITIAL_HEAT_DISPLAY"), (int) getInitialHeat());
		}
		return result;
	}

	@Override
	public double getVentCoolingCapacity() {
		return 6;
	}

	@Override
	public MaterialsList getMaterials() {
		return MATERIALS;
	}

}
