package Ic2ExpReactorPlanner.items.heatexchangers;

import Ic2ExpReactorPlanner.MaterialsList;
import Ic2ExpReactorPlanner.TextureFactory;

/**
 * Represents a heat exchanger.
 *
 * @author Brian McCloud
 */
public class ComponentHeatExchanger extends AbstractHeatExchanger {

	/**
	 * The filename for the image to show for the component.
	 */
	private static final String imageFilename = "reactorHeatSwitchSpread.png";     //NOI18N

	public static final MaterialsList MATERIALS = new MaterialsList(HeatExchanger.MATERIALS, 4, java.util.ResourceBundle.getBundle("Ic2ExpReactorPlanner/Bundle").getString("GOLD"));

	private static final int switchSide = 36;
	private static final int switchReactor = 0;

	/**
	 * Creates a new instance.
	 */
	public ComponentHeatExchanger() {
		setImage(TextureFactory.getImage(imageFilename));
		setMaxHeat(5000);
		automationThreshold = 4500;
	}

	/**
	 * Gets the name of the component.
	 *
	 * @return the name of this component.
	 */
	@Override
	public String toString() {
		String result = java.util.ResourceBundle.getBundle("Ic2ExpReactorPlanner/Bundle").getString("COMPONENT HEAT EXCHANGER");
		if (getInitialHeat() > 0) {
			result += String.format(java.util.ResourceBundle.getBundle("Ic2ExpReactorPlanner/Bundle").getString("INITIAL_HEAT_DISPLAY"), (int) getInitialHeat());
		}
		return result;
	}

	@Override
	protected int getSwitchSide() {
		return switchSide;
	}

	@Override
	protected int getSwitchReactor() {
		return switchReactor;
	}

	@Override
	public MaterialsList getMaterials() {
		return MATERIALS;
	}

}
