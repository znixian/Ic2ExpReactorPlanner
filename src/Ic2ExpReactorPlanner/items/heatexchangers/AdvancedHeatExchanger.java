package Ic2ExpReactorPlanner.items.heatexchangers;

import Ic2ExpReactorPlanner.MaterialsList;
import Ic2ExpReactorPlanner.Reactor;
import Ic2ExpReactorPlanner.items.ReactorComponent;
import Ic2ExpReactorPlanner.TextureFactory;
import static Ic2ExpReactorPlanner.items.heatexchangers.HeatExchanger.MATERIALS;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents an advanced heat exchanger.
 * @author Brian McCloud
 */
public class AdvancedHeatExchanger extends AbstractHeatExchanger {
    
    /**
     * The filename for the image to show for the component.
     */
    private static final String imageFilename = "reactorHeatSwitchDiamond.png";     //NOI18N
    
    public static final MaterialsList MATERIALS = new MaterialsList(2, HeatExchanger.MATERIALS, 2, MaterialsList.ELECTRONIC_CIRCUIT, java.util.ResourceBundle.getBundle("Ic2ExpReactorPlanner/Bundle").getString("COPPER"), 4, java.util.ResourceBundle.getBundle("Ic2ExpReactorPlanner/Bundle").getString("LAPIS LAZULI"));
    
    private static final int switchSide = 24;
    private static final int switchReactor = 8;
    
    /**
     * Creates a new instance.
     */
    public AdvancedHeatExchanger() {
        setImage(TextureFactory.getImage(imageFilename));
        setMaxHeat(10000);
    }
    
    /**
     * Gets the name of the component.
     * @return the name of this component.
     */
    @Override
    public String toString() {
        String result = java.util.ResourceBundle.getBundle("Ic2ExpReactorPlanner/Bundle").getString("ADVANCED HEAT EXCHANGER");
        if (getInitialHeat() > 0) {
            result += String.format(java.util.ResourceBundle.getBundle("Ic2ExpReactorPlanner/Bundle").getString("INITIAL_HEAT_DISPLAY"), (int)getInitialHeat());
        }
        return result;
	}

	@Override
	protected int getSwitchSide() {
		return switchSide;
	}

	@Override
	protected int getSwitchReactor() {
		return switchReactor;
	}

	@Override
	public MaterialsList getMaterials() {
		return MATERIALS;
	}
    
}
