/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ic2ExpReactorPlanner.items.heatexchangers;

import Ic2ExpReactorPlanner.Reactor;
import Ic2ExpReactorPlanner.items.ReactorComponent;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author znix
 */
public abstract class AbstractHeatExchanger extends ReactorComponent {

	private double lastHeat;

	@Override
	public boolean hasStateChanged() {
		return lastHeat == currentHeat;
	}

	@Override
	public void preReactorTick() {
		super.preReactorTick();
		lastHeat = currentHeat;
	}

	@Override
	public boolean isHeatAcceptor() {
		return !isBroken();
	}

	@Override
	public void transfer() {
		final Reactor parentReactor = getParent();
		int switchSide = getSwitchSide();
		int switchReactor = getSwitchReactor();

		List<ReactorComponent> heatableNeighbors = new ArrayList<>(4);
		ReactorComponent component = parentReactor.getComponentAt(getRow(), getColumn() - 1);
		if (component != null && component.isHeatAcceptor()) {
			heatableNeighbors.add(component);
		}
		component = parentReactor.getComponentAt(getRow(), getColumn() + 1);
		if (component != null && component.isHeatAcceptor()) {
			heatableNeighbors.add(component);
		}
		component = parentReactor.getComponentAt(getRow() - 1, getColumn());
		if (component != null && component.isHeatAcceptor()) {
			heatableNeighbors.add(component);
		}
		component = parentReactor.getComponentAt(getRow() + 1, getColumn());
		if (component != null && component.isHeatAcceptor()) {
			heatableNeighbors.add(component);
		}
		// Code adapted from decompiled IC2 code, class ItemReactorHeatSwitch, with permission from Thunderdark.
		double myHeat = 0;
		for (ReactorComponent heatableNeighbor : heatableNeighbors) {
			double mymed = getCurrentHeat() * 100.0 / getMaxHeat();
			double heatablemed = heatableNeighbor.getCurrentHeat() * 100.0 / heatableNeighbor.getMaxHeat();

			double add = (int) (heatableNeighbor.getMaxHeat() / 100.0 * (heatablemed + mymed / 2.0));
			if (add > switchSide) {
				add = switchSide;
			}
			if (heatablemed + mymed / 2.0 < 1.0) {
				add = switchSide / 2;
			}
			if (heatablemed + mymed / 2.0 < 0.75) {
				add = switchSide / 4;
			}
			if (heatablemed + mymed / 2.0 < 0.5) {
				add = switchSide / 8;
			}
			if (heatablemed + mymed / 2.0 < 0.25) {
				add = 1;
			}
			if (Math.round(heatablemed * 10.0) / 10.0 > Math.round(mymed * 10.0) / 10.0) {
				add -= 2 * add;
			} else if (Math.round(heatablemed * 10.0) / 10.0 == Math.round(mymed * 10.0) / 10.0) {
				add = 0;
			}
			myHeat -= add;
			add = heatableNeighbor.adjustCurrentHeat(add);
			myHeat += add;
		}
		double mymed = getCurrentHeat() * 100.0 / getMaxHeat();
		double Reactormed = parentReactor.getCurrentHeat() * 100.0 / parentReactor.getMaxHeat();

		int add = (int) Math.round(parentReactor.getMaxHeat() / 100.0 * (Reactormed + mymed / 2.0));
		if (add > switchReactor) {
			add = switchReactor;
		}
		if (Reactormed + mymed / 2.0 < 1.0) {
			add = switchSide / 2;
		}
		if (Reactormed + mymed / 2.0 < 0.75) {
			add = switchSide / 4;
		}
		if (Reactormed + mymed / 2.0 < 0.5) {
			add = switchSide / 8;
		}
		if (Reactormed + mymed / 2.0 < 0.25) {
			add = 1;
		}
		if (Math.round(Reactormed * 10.0) / 10.0 > Math.round(mymed * 10.0) / 10.0) {
			add -= 2 * add;
		} else if (Math.round(Reactormed * 10.0) / 10.0 == Math.round(mymed * 10.0) / 10.0) {
			add = 0;
		}
		myHeat -= add;
		parentReactor.adjustCurrentHeat(add);
		adjustCurrentHeat(myHeat);
	}

	protected abstract int getSwitchSide();

	protected abstract int getSwitchReactor();

}
