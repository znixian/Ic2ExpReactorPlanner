package Ic2ExpReactorPlanner.items.heatexchangers;

import Ic2ExpReactorPlanner.MaterialsList;
import Ic2ExpReactorPlanner.TextureFactory;

/**
 * Represents a heat exchanger.
 *
 * @author Brian McCloud
 */
public class HeatExchanger extends AbstractHeatExchanger {

	/**
	 * The filename for the image to show for the component.
	 */
	private static final String imageFilename = "reactorHeatSwitch.png";     //NOI18N

	public static final MaterialsList MATERIALS = new MaterialsList(MaterialsList.ELECTRONIC_CIRCUIT, 3, java.util.ResourceBundle.getBundle("Ic2ExpReactorPlanner/Bundle").getString("TIN"), 5, java.util.ResourceBundle.getBundle("Ic2ExpReactorPlanner/Bundle").getString("COPPER"));

	private static final int switchSide = 12;
	private static final int switchReactor = 4;

	/**
	 * Creates a new instance.
	 */
	public HeatExchanger() {
		setImage(TextureFactory.getImage(imageFilename));
		setMaxHeat(2500);
		automationThreshold = 2250;
	}

	/**
	 * Gets the name of the component.
	 *
	 * @return the name of this component.
	 */
	@Override
	public String toString() {
		String result = java.util.ResourceBundle.getBundle("Ic2ExpReactorPlanner/Bundle").getString("HEAT EXCHANGER");
		if (getInitialHeat() > 0) {
			result += String.format(java.util.ResourceBundle.getBundle("Ic2ExpReactorPlanner/Bundle").getString("INITIAL_HEAT_DISPLAY"), (int) getInitialHeat());
		}
		return result;
	}

	@Override
	protected int getSwitchSide() {
		return switchSide;
	}

	@Override
	protected int getSwitchReactor() {
		return switchReactor;
	}

	@Override
	public MaterialsList getMaterials() {
		return MATERIALS;
	}

}
