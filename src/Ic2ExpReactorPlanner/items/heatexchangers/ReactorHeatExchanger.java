package Ic2ExpReactorPlanner.items.heatexchangers;

import Ic2ExpReactorPlanner.MaterialsList;
import Ic2ExpReactorPlanner.TextureFactory;

/**
 * Represents a reactor heat exchanger.
 *
 * @author Brian McCloud
 */
public class ReactorHeatExchanger extends AbstractHeatExchanger {

	/**
	 * The filename for the image to show for the component.
	 */
	private static final String imageFilename = "reactorHeatSwitchCore.png";     //NOI18N

	public static final MaterialsList MATERIALS = new MaterialsList(HeatExchanger.MATERIALS, 8, java.util.ResourceBundle.getBundle("Ic2ExpReactorPlanner/Bundle").getString("COPPER"));

	private static final int switchSide = 0;
	private static final int switchReactor = 72;

	/**
	 * Creates a new instance.
	 */
	public ReactorHeatExchanger() {
		setImage(TextureFactory.getImage(imageFilename));
		setMaxHeat(5000);
		automationThreshold = 4500;
	}

	/**
	 * Gets the name of the component.
	 *
	 * @return the name of this component.
	 */
	@Override
	public String toString() {
		String result = java.util.ResourceBundle.getBundle("Ic2ExpReactorPlanner/Bundle").getString("REACTOR HEAT EXCHANGER");
		if (getInitialHeat() > 0) {
			result += String.format(java.util.ResourceBundle.getBundle("Ic2ExpReactorPlanner/Bundle").getString("INITIAL_HEAT_DISPLAY"), (int) getInitialHeat());
		}
		return result;
	}

	@Override
	protected int getSwitchSide() {
		return switchSide;
	}

	@Override
	protected int getSwitchReactor() {
		return switchReactor;
	}

	@Override
	public MaterialsList getMaterials() {
		return MATERIALS;
	}

}
