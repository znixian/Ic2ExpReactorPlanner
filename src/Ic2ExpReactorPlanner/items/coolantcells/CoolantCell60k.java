package Ic2ExpReactorPlanner.items.coolantcells;

import Ic2ExpReactorPlanner.MaterialsList;
import Ic2ExpReactorPlanner.TextureFactory;

/**
 * Represents a 60k Coolant Cell.
 *
 * @author Brian McCloud
 */
public class CoolantCell60k extends AbstractCoolantCell {

	/**
	 * The filename for the image to show for the component.
	 */
	private static final String imageFilename = "reactorCoolantSix.png";     //NOI18N

	public static final MaterialsList MATERIALS = new MaterialsList(2, CoolantCell30k.MATERIALS, java.util.ResourceBundle.getBundle("Ic2ExpReactorPlanner/Bundle").getString("IRON"), 6, java.util.ResourceBundle.getBundle("Ic2ExpReactorPlanner/Bundle").getString("TIN"));

	/**
	 * Creates a new instance.
	 */
	public CoolantCell60k() {
		setImage(TextureFactory.getImage(imageFilename));
		setMaxHeat(60000);
		automationThreshold = 54000;
	}

	/**
	 * Gets the name of the component.
	 *
	 * @return the name of this component.
	 */
	@Override
	public String toString() {
		String result = java.util.ResourceBundle.getBundle("Ic2ExpReactorPlanner/Bundle").getString("60K COOLANT CELL");
		if (getInitialHeat() > 0) {
			result += String.format(java.util.ResourceBundle.getBundle("Ic2ExpReactorPlanner/Bundle").getString("INITIAL_HEAT_DISPLAY"), (int) getInitialHeat());
		}
		return result;
	}

	@Override
	public MaterialsList getMaterials() {
		return MATERIALS;
	}

}
