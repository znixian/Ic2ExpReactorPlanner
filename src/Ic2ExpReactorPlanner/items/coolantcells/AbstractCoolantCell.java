/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ic2ExpReactorPlanner.items.coolantcells;

import Ic2ExpReactorPlanner.MaterialsList;
import Ic2ExpReactorPlanner.items.ReactorComponent;

/**
 *
 * @author znix
 */
public abstract class AbstractCoolantCell extends ReactorComponent {

	@Override
	public boolean isHeatAcceptor() {
		return !isBroken();
	}

	@Override
	public abstract MaterialsList getMaterials();

	@Override
	public double adjustCurrentHeat(double heat) {
		currentCellCooling += heat;
		bestCellCooling = Math.max(currentCellCooling, bestCellCooling);
		return super.adjustCurrentHeat(heat);
	}

	@Override
	public boolean hasStateChanged() {
		return currentCellCooling != 0;
	}
}
